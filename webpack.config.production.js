const webpack = require('webpack');
const webpackCopy = require('copy-webpack-plugin');
const path = require("path");
const WebpackCopyAfterBuildPlugin = require("webpack-copy-after-build-plugin");
module.exports = {
    entry: {
        lift: "./src/app/index.tsx"
    },    
    output: {
        filename: "[name].bundle.js",
        path: path.resolve(__dirname, "dist"),
        libraryTarget: 'amd'
    },
    node: {
        fs: "empty"
    },
    plugins: [
        new WebpackCopyAfterBuildPlugin({
            "lifts":
            "../../public/lifts.bundle.js",
          })    
    ],

    devtool: "#cheap-eval-source-map",

    resolve: {
        modules: ['node_modules', 'src'],
        extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".css"]
    },

    module: {
        loaders: [
            { test: /\.tsx?$/, loader: "awesome-typescript-loader" },
            { test: /\.(jpe?g|gif|png|svg|woff|ttf|eot|wav|mp3)$/, use: "file-loader" }
        ],

    },
    externals:{
        "react": "react",
        "react-dom": "react-dom"
    }
};