import * as React from 'react';
import Packs from './../modules/packsForm/container';
import Landing from './../modules/landing/container';

interface ComponentClass<P, S> {
  new(props?: P, state?: S): React.Component<P, S>;
}

interface Flow {
  [key: string]: ComponentClass<object, object>;
}

const flow: any = {
  liftsFlow: {
    packs: Packs,
    landing: Landing
  }
};

export default flow;