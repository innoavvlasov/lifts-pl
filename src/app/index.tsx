import * as React from 'react';
import { connect } from 'react-redux';
import processes from './processes';
import { reducer as reduxFormReducer } from 'redux-form';

interface InitFlow {
  flowName: string;
  url: string;
}
interface AppProps {
  initFlow: (properties: InitFlow, data?: any) => void;
  flowName?: string;
  stateName?: string;
  isLoading: boolean;
  workflowData: any;
}
interface State {
  initFlow?: any;
  flowName?: string;
}

class App extends React.Component<AppProps, any> {
  public componentDidMount() {
       this.props.initFlow({flowName: 'liftsFlow', url: 'initflow'});
  }

  render() {
    let { workflowData: {flowName, stateName}, isLoading } = this.props;
    let Component;
    if (flowName && stateName && !isLoading) {
      Component = processes[flowName][stateName];
    }
    return (
      <div>
        {!Component && <div>Loading...</div> 
          || <Component {...this.props} />
        }
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
    
});

const mapDispatchToProps = (dispatch: any): {} => ({});

let connectedApp = connect(mapStateToProps, mapDispatchToProps)(App as any);
let reducers = 'reducer';
connectedApp[reducers] = reduxFormReducer;

export default connectedApp;
