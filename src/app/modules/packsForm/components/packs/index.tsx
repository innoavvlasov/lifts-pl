import * as React from 'react';

import { dictionary } from '../../constants';
import PackageProps from '../../interfaces';
import {
    Package, Pname, Hr,
    Auditory, Audtext,
    GrayPackText, Centr,
    Devider, Numbers
} from './style';
import { AddrButton, BucketButton } from 'mediapult-ui';
import '../../helpers';

export const Pack = (props: PackageProps) => {
    let {rating, district, package_id,
        auditory, package_name, cost,
        page_format, lifts_available, company_name} = props;
    package_name = package_name.charAt(0).toUpperCase() + package_name.substr(1);
    console.info(`pack ${props.package_id}`, props);
    const packClickHandle = () => {
        console.info('clicked', package_id, props);
    };
    return (
        <Package id={package_id} onClick={packClickHandle}>
            <Pname>{package_name}</Pname>
            <Auditory>{auditory.formatAuditory(0, '.', ',')}</Auditory>
            <Audtext>{dictionary.audText}</Audtext><Hr/>
            <GrayPackText>{dictionary.costTextPrefix}
                <Numbers>{cost}</Numbers>{dictionary.costTextPostfix}</GrayPackText><br/>
            <GrayPackText>{dictionary.formatPrefix}
                <Numbers>{page_format}</Numbers>
            </GrayPackText><br/>
            <GrayPackText>{dictionary.liftsPrefix}
                <Numbers>{lifts_available}</Numbers>{dictionary.liftsPostfix}</GrayPackText><br/>
            <GrayPackText>{dictionary.companyPrefix}
                <Numbers>{
                    company_name.length > 21 ?
                        company_name.slice(0, 19) + '...' :
                        company_name
                    }</Numbers>
            </GrayPackText><br/>
            <GrayPackText>{dictionary.districtTextPrefix}
                <Numbers>{district.label}</Numbers>
            </GrayPackText><br/>
            <Centr>
                <AddrButton active={true}> {dictionary.address} </AddrButton>
                <Devider/>
                <BucketButton active={true} value={dictionary.inBucket}/>
            </Centr>
        </Package>
    );
};