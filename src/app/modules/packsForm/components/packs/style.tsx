import styled from 'styled-components';
import { Colors } from '../../constants';

export const Package = styled.div`
  font-family: ProximaNova;
  color: black;
  display: inline-block;
  width: 270px;
  height: 315px;
  background: white;
  /* margin: 10px 20px 20px; */
  border: 1px solid #eaeaea;
`;
export const Pname = styled.div`
  color: ${Colors.bluish};
  font-weight: bold;
  width: fit-content;
  margin: 25px auto 0;
`;
export const Auditory = styled.div`
  width: fit-content;
  margin: 0 auto;
  font-weight: bold;
  font-size: 50px;
  color: ${Colors.mainColor};
`;
export const Audtext = styled.div`
  width: fit-content;
  margin: 0 auto;
  color: ${Colors.notActiveColor};
`;
export const Hr = styled.hr`
  margin: 5px 0;
`;
export const GrayPackText = styled.div`
  display: inline;
  width: fit-content;
  margin-left: 17px;
  color: ${Colors.grayColor};
`;
export const Numbers = styled.div`
  display: inline;
  color: black;
  font-weight: bold;
`;
export const Centr = styled.div`
  margin: 0 auto;
  width: fit-content;
`;
export const Devider = styled.div`
  display: inline-block;
  width: 10px;
`;