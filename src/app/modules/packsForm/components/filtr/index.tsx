import * as React from 'react';
import * as DOM from 'react-dom';
import {
    formValueSelector,
    reduxForm
  } from 'redux-form';

import Selecter from '../select';
import { Checkbox, DropDown, RangeSelector, RangeSelector2 } from 'mediapult-ui';
import {
    FiltrColor,
    WhiteWrap,
    Text,
    ResetDiv,
    Circle
} from './styled';
import { dictionary } from '../../constants';
import { changeFormatsSelector } from '../../selectors';

const Reset = ({action}: {action: () => {}}) => {
    return(
        <ResetDiv onClick={action}>
            {dictionary.reset}
            <Circle>x</Circle>
        </ResetDiv>
    );
};

const toogleFiltr = (array, cheked, val) => {
    let newArray = [];
    array = array == null ? [] : array;
    const condition = (value) => value !== val;
    if (cheked) {
        newArray.push(val);
    } else {
        array = array.filter(condition);
    }
    // console.info('newArray', newArray, ' array', array);
    return [...newArray, ...array];
};

const Filter = (props: any) => {
    let { change } = props;
    let { filters, statistic, city: { districts } } = props.liftData;
    let options: any = districts.map((value: any) => {return {...value, value: value.label}; });
    return (
        <WhiteWrap>
            <Text>{dictionary.district}</Text>
            {/* <DropDown {...props} /> */}
            <Selecter
                options={options}
                onChange={(index, value) => change('filters.districts', value)}
            />
            <Text>{dictionary.formats}</Text>
            <Checkbox
                onChange={(index, isCheked) => change('filters.formats', toogleFiltr(filters.formats, isCheked, 'A4'))}
                index={0}
                name="A4"
            />
            <Checkbox
                onChange={(index, isCheked) => change('filters.formats', toogleFiltr(filters.formats, isCheked, 'A3'))}
                name="A3"
                index={1}
            />
            <Checkbox
                onChange={(index, isCheked) => change('filters.formats', toogleFiltr(filters.formats, isCheked, 'A2'))}
                name="A2"
                index={2}
            />
            <Text>{dictionary.auditory}</Text>
            {/*<RangeSelector drop={props.SetAuditory} target={'cost'} maxValue={100000} />*/}
            <RangeSelector2
                maxValue={filters.auditory.to}
                minValue={filters.auditory.from}
                action={props.filterSetAuditory}
            />
            <Text>{dictionary.cost}</Text>
            <RangeSelector2
                maxValue={filters.cost.to}
                minValue={filters.cost.from}
                action={props.filterSetCost}
            />
            {/*<RangeSelector drop={props.SetAuditory} target={'auditory'}  maxValue={55000} />*/}
            <Reset action={props.filterResetDefault} />
        </WhiteWrap>
    );
};
export default Filter;