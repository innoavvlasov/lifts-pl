import styled from 'styled-components';

export const FiltrColor = Object.freeze({
  bluish: '#848c98',
  blue: '#2ea2f8',
  gray: '#ced0da',
  red: 'red'
});

export const WhiteWrap = styled.div`
  background: white;
  /* width: 270px; */
  height: 480px;
  /* margin-top: 10px; */
  padding: 20px;
`;
export const Text = styled.div`
  font-size: 12px;
  color: ${FiltrColor.bluish};
  font-weight: 700;
  text-transform: uppercase;
  margin-top: 20px;
`;

export const ResetDiv = styled.div`
  cursor: pointer;
  width: fit-content;
  margin: 20px auto;
  text-transform: uppercase;

  &:hover {
      text-decoration: underline;

    > div {
      background-color: ${FiltrColor.red};
    }
  }
`;

export const Circle = styled.div`
  margin-left: 5px;
  width: 16px;
  height: 16px;
  border-radius: 8px;
  display: inline-block;
  background: ${FiltrColor.gray};
  color: white;
  text-align: center;
  line-height: 17px;
`;
