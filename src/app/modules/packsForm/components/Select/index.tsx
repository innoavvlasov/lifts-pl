import * as React from 'react';

import { Option, OuterList, Input, DropDownIcon, Wrapper } from './styled';

export default class Select extends React.Component<any, any> {
  selectValue: string;
  propsOptions: any = [];
  options: any;
  constructor(props: any) {
    super(props);
    this.hide = this.hide.bind(this);
    this.toogleOuterList = this.toogleOuterList.bind(this);
    this.selected = this.selected.bind(this);

    if (props.range) {
      for (var i = props.range.start; i <= props.range.end; i++) {
        this.propsOptions.push({ label: i, value: i });
      }
    }
    this.propsOptions = this.props.options
      ? this.props.options
      : this.propsOptions;
    this.options = this.propsOptions.map((item, index) => {
      return (
        <Option key={index} value={item.value}>
          <a href="#" onClick={() => this.selected(event, index, item.value)}>
            {item.label}
          </a>
        </Option>
      );
    });
    
    this.selectValue = this.propsOptions[0].value;
    let defState = { show: false };
    this.state = { ...this.state, ...defState };
  }

  selected(e: any, index: number, value: string) {
    e.preventDefault();
    this.selectValue = value;
    this.props.onChange(index, value);
    this.setState({ selectValue: value });
  }

  toogleOuterList(value: any) {
    this.setState({ show: !value });
  }

  hide(e: any) {
    if (e && e.relatedTarget) {
      e.relatedTarget.click();
    }
    this.setState({ show: false });
  }

  render() {
    return (
      <Wrapper {...this.props} value={this.selectValue}>
        <Input
          className={this.state.show ? 'bordered' : ''}
          type="button"
          onBlur={this.hide}
          onClick={() => this.toogleOuterList(this.state.show)}
        >
         <div>{this.selectValue}</div>
         <div><DropDownIcon /></div>
        </Input>
        {this.state !== null &&
          this.state.show && <OuterList>{this.options}</OuterList>}
      </Wrapper>
    );
  }
}
