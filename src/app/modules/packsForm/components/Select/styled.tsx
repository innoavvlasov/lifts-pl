import * as React from 'react';
import styled from 'styled-components';

export const Wrapper: any = styled.div`
  user-select: none;
  cursor: pointer;
  position: relative;
  
  .bordered {
    border-color: rgb(46, 162, 248);
  }
`;

export const Option = styled.li`
  list-style-type:none;
  font-family: 'ProximaNovaSemibold';
  cursor: pointer;
  display: block;
  font-size: 14px;
  color: rgb(53, 64, 82);
  
  &:hover {
    color: #2ea2f8;
    background-color: #f1f4f8;
  }
  border-bottom: 1px solid rgb(223, 227, 233);

  &:last-child {
    border-bottom: none;
  }
  
  a {
    padding: 3px 15px;
    display: inline-block;
  }

  a:link {
    text-decoration: none;
  }

  a:visited {
      text-decoration: none;
  }
`;

export const OuterList = styled.ul`
  position: absolute;
  border-width: 1px;
  border-color: rgb(46, 162, 248);
  border-style: solid;
  background-color: rgb(255, 255, 255);
  box-shadow: 0px 1px 4px 0px rgba(0, 0, 0, 0.08);
  border-bottom-right-radius: 4px;
  border-bottom-left-radius: 4px;
  min-width: 65px;
  z-index: 1;
  left: 20px;
  top: 28px;
`;

export const Input = styled.button`
  border: 1px solid rgb(206, 208, 218);
  border-radius: 4px;
  background-image: -moz-linear-gradient( 90deg, rgb(242,244,247) 0%, rgb(254,255,255) 100%);
  background-image: -webkit-linear-gradient( 90deg, rgb(242,244,247) 0%, rgb(254,255,255) 100%);
  background-image: -ms-linear-gradient( 90deg, rgb(242,244,247) 0%, rgb(254,255,255) 100%);
  font-size: 14px;
  color: #354052;
  display: inline-block;
  position: relative;

  > div:first-child{
    float: left;
    width: calc(100% - 53px);
    padding: 5px 15px;
  }

  > div:last-child{
    float: left;
    width: 16px;
    padding-top: 3px;
    padding-right: 7px;
  }

  &:focus {
    outline: none;
  }
`;

const DropDownSVG = styled.svg`
  width: 8px;
  height: 4px;
`;

export const DropDownIcon = (props: any) => {
    return (
    <DropDownSVG {...props} viewBox="0 0 8 4">
        <path
            fill-rule="evenodd"
            fill="rgb(168, 170, 183)"
// tslint:disable-next-line:max-line-length
            d="M7.253,0.935 L4.323,3.809 C4.106,4.023 3.754,4.023 3.537,3.809 L0.607,0.935 C0.390,0.721 0.390,0.374 0.607,0.160 C0.824,-0.054 1.176,-0.054 1.393,0.160 L3.930,2.649 L6.467,0.160 C6.684,-0.054 7.035,-0.054 7.253,0.160 C7.469,0.374 7.469,0.721 7.253,0.935 Z"
        />
   </DropDownSVG>
    );
};