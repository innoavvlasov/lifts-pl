import * as React from 'react';
import { Checkbox, Newcheckbox, AddrButton } from 'mediapult-ui';
import Package, { District } from '../../interfaces';
import { Pack } from '../packs';
import Filter from '../filtr';
import { dictionary, Colors } from '../../constants';
import { AreaWrapper } from './styled';
import { filtersAndSortPacks } from '../../helpers';
// TODO: заменить кнопку more на автоматическую подгрузку данных

const WorkArea = (props: any) => {
  let { packs, error, filters, packsOnScreen } = props.liftData;
  let data = (packs) ? packs : [];
  // data = filtersAndSortPacks(data, filters);
  // tslint:disable-next-line:jsdoc-format
  /**Фильтрация же на стороне сервера :( */
  return (
    <AreaWrapper>
      <Filter {...props} />
      <div>
        {(data.length > 0) && data
          .slice(0, packsOnScreen)
          .map((pack: Package, index: number) => <Pack
            {...pack}
            district={!!props.districts ?
              props.districts.find((dist: District) => dist.id === pack.district_id) :
              'неизвестно'}
            key={index}
          />) ||
          <div>{error}</div>
          }
          <button onClick={props.showMorePacks}>{dictionary.more}</button>
        </div>
    </AreaWrapper>
  );
};

export default WorkArea;