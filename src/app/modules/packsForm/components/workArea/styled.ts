import styled from 'styled-components';

export const AreaWrapper = styled.div `
  display: grid;
  grid-column-gap: 30px;
  grid-template-columns: 270px auto;
  justify-content: start;  
  @media only screen and (max-width: 3840px) and (min-width: 1600px) {
    grid-template-columns: 270px 1100px;
  }
  /* max-width: 1170px;
  margin: 0 auto; */
`;