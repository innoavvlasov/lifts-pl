import * as React from 'react';
import WorkArea from './workArea';
import { WrapperForm, BaseForm } from 'mediapult-ui';

const View = (props: any) => {
    return (
        <WrapperForm {...props}>
          <BaseForm
            city={'Хабаровск'}
            breadcrumbs={props.liftData.breadcrumbs}
            stateName={'packs'}
          >
            <WorkArea {...props}/> 
          </BaseForm>
        </WrapperForm>
      );
};

export default View;