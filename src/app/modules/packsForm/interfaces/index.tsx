interface Array <T> {
    find(predicate: (search: T) => boolean): T;
}
export interface CheckboxProps {
    name: string;
    change: (item: string) => void;
    formats: Array <string>;
}
export interface RangeProps {
    minValue: number;
    maxValue: number;
    action: (from: number, to: number) => void;
}

interface PackageProps {
    package_id: string;
    package_name: string;
    district_id: number;
    district: District;
    rating: number;
    auditory: number;
    cost: number;
    page_format: string;
    lifts_available: number;
    company_name: string;
    address: boolean;
}
export default PackageProps;

export interface Range {
    from: number;
    to: number;
}

interface Array <T> {
    length: number;
    find(predicate: (search: T) => boolean): T;
    filter(predicate: (search: T) => boolean): T;
    push(input: T): void;
}

export interface State {
    packs: any;
    fetching: boolean;
    error: string;
    packsOnScreen: number;
    cityInfo: {};
    cityInfoFetching: boolean;
    filter: Filter;
    statistic: Statistic;
}

interface City {
    id: number;
    city_name: string;
    subject: string;
    population: number;
    districts: District;
}

interface Payload {
    packs?: any;
    pack?: any;
    format?: string;
    districts?: string;
    district?: string;
    error?: string;
    city?: City;
    from?: number;
    to?: number;
    statistic?: Statistic;
}
interface Statistic {
    auditoryMax: number;
    auditoryMin: number;
    costMax: number;
    costMin: number;
}

export interface Action {
    type: string;
    payload?: Payload;
}

export interface MinMax {
    min: number;
    max: number;
}

// новый интерфейс на статистику с бэка
// export interface Statistic {
//     auditory: MinMax;
//     cost: MinMax;
//     formats: [string];
// }

export interface Pack {
  package_id: number;
  district_id: number;
  package_name: string;
  auditory: number;
  cost: number;
  page_format: string;
  lifts_available: number;
  company_name: string;
  address: boolean;
  rating: number;
}

export interface Filter {
    // tslint:disable-next-line:jsdoc-format
    /**только id дистрикта по которому нало сортировать */
    districts: number[];
    formats?: string[];
    auditory: Range;
    cost: Range;
}

export interface District {
    id: number;
    label: string;
    population: number;
}