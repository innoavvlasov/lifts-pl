const dictionary = Object.freeze({
    address: 'Адрес',
    inBucket: 'В корзину',
    district: 'район',
    districts: 'Районы',
    formats: 'форматы:',
    auditory: 'аудитория',
    cost: 'цена',
    reset: 'сбросить все фильтры',
    audText: 'аудитория чел',
    costTextPrefix: 'Стоимость: ',
    costTextPostfix: ' руб/месяц.',
    formatPrefix: 'Формат: ',
    liftsPrefix: 'Лифты: ',
    liftsPostfix: ' шт.',
    companyPrefix: 'Компания: ',
    districtTextPrefix: 'Район: ',
    more: 'more'
});

const constants = Object.freeze({
    packsAtStart: 9,
    addPacksOnScreen: 9
});

export const FORM_FIELD_CHANGE = 'FORM_FIELD_CHANGE';
export const FORM_FIELDS_CLEAR = 'FORM_FIELDS_CLEAR';
// export const TOGGLE = 'TOGGLE';
// export const SET_FORM_DATA = 'SET_FORM_DATA';
// export const FILTER_LIST_OPEN = 'FILTER_LIST_OPEN';
// export const GET_PACKS_REQUEST = 'GET_PACKS_REQUEST';
// export const GET_PACKS_SUCCESS = 'GET_PACKS_SUCCESS';
// export const GET_PACKS_STATISTIC = 'GET_PACKS_STATISTIC';
// export const GET_PACKS_ERROR = 'GET_PACKS_ERROR';
// export const GET_CITY_INFO_REQUEST = 'GET_CITY_INFO_REQUEST';
// export const GET_CITY_INFO_SUCSESS = 'GET_CITY_INFO_SUCSESS';
export const SHOW_MORE_PACKS = 'SHOW_MORE_PACKS';

export const FILTER_TOGGLE_DISTRICT = 'FILTR_TOGGLE_DISTRICT';
export const FILTER_TOGGLE_FORMAT = 'FILTR_TOGGLE_FORMAT';
export const FILTER_CHANGE_AUDITORY = 'FILTR_CHANGE_AUDITORY';
export const FILTER_CHANGE_COST = 'FILTR_CHANGE_COST';
export const FILTER_RESET_DEFAULT = 'FILTR_RESET_DEFAULT';

const Colors = Object.freeze({
    mainColor: '#2ea2f8',
    notActiveColor: '#ced0da',
    grayColor: '#848c98',
    white: 'white',
    orangeColor: '#ff7800',
    graishListBg: '#f1f4f8',
    alternativeGraish: '#eff3f6',
    bluish: '#354052',
});
export {Colors, constants, dictionary};
