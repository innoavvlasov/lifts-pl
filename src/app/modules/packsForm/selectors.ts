import { getStatistic, toggleInStringArray } from './helpers';

export const getAllInfo = (data: any) => {
    return {
        statistic: data.statistic,
        packs: data.packs,
        filters: {
          auditory: {
              from: data.statistic.auditory.min,
              to: data.statistic.auditory.max
          },
          cost: {
              from: data.statistic.cost.min,
              to: data.statistic.cost.max
          },
          districts: new Array(),
          formats: new Array()
        },
        city: data.city
    };
};

export const getPacksSelector = (data: any) => {
    const stat = getStatistic(data);
    return {        
        statistic: stat,
        packs: data,
        filters: {
          auditory: {
              from: stat.auditoryMin,
              to: stat.auditoryMax
          },
          cost: {
              from: stat.costMin,
              to: stat.costMax
          },
          districts: new Array(),
          formats: new Array()
      }
    };
};

export const changeFormatsSelector = (filters: any, data: string) => {
    return {
        filters: {
          ...filters,
          formats: toggleInStringArray(filters.formats, data)
      }
    };
};