import * as React from 'react';
import { connect } from 'react-redux';
import { getPacksSelector, getAllInfo } from './selectors';
import View from './components/PacksView';
import { Filter, Pack } from './interfaces';
import {
  formValueSelector,
  reduxForm
} from 'redux-form';
import { initialState } from 'app/modules/packsForm/initial-state';

const form = 'liftForm';
const selector = formValueSelector(form);

const mapStateToProps = (state: PacksState): PacksInitForm => {
  let wfData = state.workflow.data;
  return {
    liftData: {
      breadcrumbs: wfData.breadcrumbs,
      packs: wfData.packs,
      packsOnScreen: 9,
      statistic: {
        formats: wfData.statistic.formats,
        auditory: {
            min: wfData.statistic.auditory.min,
            max: wfData.statistic.auditory.max
        },
        cost: {
            min: wfData.statistic.cost.min,
            max: wfData.statistic.cost.max
        },
        packsCount: wfData.statistic.packsCount
      },
      filters: {
        districts: selector(state, 'filters.districts') || wfData.filters.districts,
        formats: selector(state, 'filters.formats') || wfData.filters.formats,
        auditory: wfData.filters.auditory,
        cost: wfData.filters.cost
      },
      city: wfData.city
    }
  };
};

const CartForm = reduxForm({
  form: form,
  initialValues: initialState
})(View);

let connectedApp = connect(mapStateToProps, null)(CartForm);
export default connectedApp;

export interface District {
  id: number;
  label: string;
  population: number;
}

interface City {
  id: number;
  city_name: string;
  subject: string;
  population: number;
  districts: District[];
}

interface MinMax {
  min: number;
  max: number;
}

interface Statistic {
  auditory: MinMax;
  cost: MinMax;
  formats: string[];
  packsCount: number;
}

export interface Breadcrumb {
  stateName: string;
  flow: string;
  title: string;
}

interface PacksInit {
  stateName: string;
  flowName: string;
  city: City;
  statistic: Statistic;
  packs: Pack[];
  breadcrumbs: Breadcrumb[];
  filters: Filter;
}

interface PacksState {
  workflow: Workflow;
}
interface Workflow {
  data: PacksInit;
}
export interface FormData {
  statistic: Statistic;
  packs: Pack[];
  breadcrumbs: Breadcrumb[];
  filters: Filter;
  city: City;
  packsOnScreen: number;
}
export interface PacksInitForm {
  liftData: FormData;
}