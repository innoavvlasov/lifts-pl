import { Filter, Pack, District } from './interfaces';
declare global {
    interface Number {
        formatAuditory: (c: number, d: string, t: string) => string;
    }
}
Number.prototype.formatAuditory = function (c: number, d: string, t: string): string {
    var n = this,
        a = isNaN(c = Math.abs(c)) ? 2 : c,
        b = d === undefined ? '.' : d,
        r: string = t === undefined ? ',' : t,
        s = n < 0 ? '-' : '',
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c), 0)),
        g: number = parseInt(i, 0),
        j: number = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + r : '') + i.substr(j)
        .replace(/(\d{3})(?=\d)/g, '$1' + r) + (a ? b + Math.abs(n - g).toFixed(a).slice(2) : '');
};

export const toggleInArray = (array: any, toggleElem: any, param: string = 'id') => {
    let ansver = array;
    if (array.length && array.length > 0) {
        if (undefined === array.find((elem: any) => elem[param] === toggleElem[param])) {
            ansver = [...ansver, toggleElem];
        } else {
            ansver = ansver.filter((elem: any) => elem[param] !== toggleElem[param]);
        }

        return ansver;
    } else {
        return [toggleElem];
    }
};

export const toggleInStringArray = (array: string[], item: string): string[] => {
    if (array.length > 0) {
        if (!array.find(arrItem => arrItem === item)) {
            array.push(item);
        } else {
            array = array.filter((arrItem) => arrItem !== item);
        }
    } else {
        array.push(item);
    }
    return array;
};

const getStatistic = (data: any) => {
    let auditoryMax: number = 0,
        auditoryMin: number = 9999,
        costMax: number = 0,
        costMin: number = 9999;

    data.forEach((pack: any) => {
        auditoryMax = Math.max(pack.auditory, auditoryMax);
        auditoryMin = Math.min(pack.auditory, auditoryMin);
        costMax = Math.max(pack.cost, costMax);
        costMin = Math.min(pack.cost, costMin);
    });

    return {
        auditoryMax,
        auditoryMin,
        costMax,
        costMin
    };
};

export { getStatistic };

export const filtersAndSortPacks = (data: Array<Pack>, filters: Filter) => {
    let result = data
    /** фильтруем по районам */
    .filter((pack: Pack) => {
        if (!!filters.districts && filters.districts.length > 0) {
            return !!filters.districts
              .find((dist: number) => pack.district_id === dist);
          } else {
            return () => true;
        }
    })
    /** фильтруем по аудитории */
    .filter((pack: Pack) => (
        pack.auditory >= filters.auditory.from && pack.auditory <= filters.auditory.to)
    )
    /** фильтруем по цене */
    .filter((pack: Pack) => (
        pack.cost >= filters.cost.from && pack.cost <= filters.cost.to)
    )
    /** фильтруем по цене */
    .filter((pack: Pack) => filters.formats.length > 0 ?
            filters.formats.find((format: string) => pack.page_format === format) : true 
    )
    /** сортировка */
    .sort((a: Pack, b: Pack) => a.rating - b.rating)
    ;

    return result;
};