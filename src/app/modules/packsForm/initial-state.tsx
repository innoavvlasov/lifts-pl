import { constants }  from './constants';
export const initialState: any = {
    packs: [],
    fetching: false,
    error: null,
    packsOnScreen: constants.packsAtStart,
    cityInfo: {},
    cityInfoFetching: false,
    filters: {
        districts: [],
        formats: [],
        auditory: {
            from: 0,
            to: 100
        },
        cost: {
            from: 0,
            to: 100
        }
    },
    statistic: {
        auditoryMax: 0,
        auditoryMin: 0,
        costMin: 0,
        costMax: 0,
        packsCount: 58
    },
    breadcrumbs: []
};