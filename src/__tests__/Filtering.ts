import { filtersAndSortPacks } from '../app/modules/packsForm/helpers';
import { Filter, District } from '../app/modules/packsForm/interfaces';

describe('Проверка фильтрации пакетов', function() {

    let packs = [{
        package_id: 1768,
        district_id: 0,
        package_name: 'бездонный',
        auditory: 797,
        cost: 1594,
        page_format: 'A3',
        lifts_available: 6,
        company_name: 'United States of AmericaKonecranes',
        address: true,
        rating: 0.21
    },
    {
        package_id: 1769,
        district_id: 1,
        package_name: 'мертвый',
        auditory: 860,
        cost: 1720,
        page_format: 'A4',
        lifts_available: 8,
        company_name: 'RussiaРемснаб-трактор',
        address: false,
        rating: 0.59
    },
    {
        package_id: 1770,
        district_id: 1,
        package_name: 'немаловажный',
        auditory: 805,
        cost: 1610,
        page_format: 'A2',
        lifts_available: 8,
        company_name: 'UkraineАгроимпорт',
        address: false,
        rating: 0.64
    }];

    let filters: Filter = {
        districts: [1],
        formats: [],
        auditory: {
            from: 188,
            to: 860
        },
        cost: {
            from: 195,
            to: 1650
        }
    };

    let answer = [{
        package_id: 1770,
        district_id: 1,
        package_name: 'немаловажный',
        auditory: 805,
        cost: 1610,
        page_format: 'A2',
        lifts_available: 8,
        company_name: 'UkraineАгроимпорт',
        address: false,
        rating: 0.64
    }];

    it('вернет пустой массив', function() {
        expect(filtersAndSortPacks([], filters)).toEqual([]);
    });
    it('Отсортирует пакеты', function() {
        expect(filtersAndSortPacks(packs, filters)).toEqual(answer);
    });
    let filters2: Filter = {
        districts: [],
        formats: ['A4', 'A3'],
        auditory: {
            from: 188,
            to: 860
        },
        cost: {
            from: 195,
            to: 5000
        }
    };
    let answer2 = [{
        package_id: 1768,
        district_id: 0,
        package_name: 'бездонный',
        auditory: 797,
        cost: 1594,
        page_format: 'A3',
        lifts_available: 6,
        company_name: 'United States of AmericaKonecranes',
        address: true,
        rating: 0.21
    },
    {
        package_id: 1769,
        district_id: 1,
        package_name: 'мертвый',
        auditory: 860,
        cost: 1720,
        page_format: 'A4',
        lifts_available: 8,
        company_name: 'RussiaРемснаб-трактор',
        address: false,
        rating: 0.59
    }];
    it('Отсортирует пакеты по форматам', function() {
        expect(filtersAndSortPacks(packs, filters2)).toEqual(answer2);
    });

    let filters3: Filter = {
        districts: [],
        formats: [],
        auditory: {
            from: 0,
            to: 5000
        },
        cost: {
            from: 0,
            to: 5000
        }
    };
    it('Вернет без изменений', function() {
        expect(filtersAndSortPacks(packs, filters3)).toEqual(packs);
    });
});