const webpack = require('webpack');
const path = require("path");
module.exports = {
    entry: {
        lift: "./src/app/index.tsx"
    },    
    output: {
        filename: "[name].bundle.js",
        path: path.resolve(__dirname, "dist/lift"),
        libraryTarget: 'amd',
        publicPath: '/lift/'
    },
    node: {
        fs: "empty"
    },

    devtool: "#cheap-eval-source-map",

    resolve: {
        modules: ['node_modules', 'src'],
        extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".css"]
    },

    module: {
        loaders: [
            { test: /\.tsx?$/, loader: "awesome-typescript-loader" },
            { test: /\.(jpe?g|gif|png|svg|woff|ttf|eot|wav|mp3)$/, use: "file-loader" }
        ],

    },
    externals:{
        "react": "react",
        "react-dom": "react-dom",
        "mediapult-ui": "mediapult-ui",
        'redux': 'redux',
        'react-redux': 'react-redux',
        'redux-form': 'redux-form'
    }
};